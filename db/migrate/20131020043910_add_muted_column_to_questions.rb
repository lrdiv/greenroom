class AddMutedColumnToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :removed_at, :datetime
  end
end
