class AddSomeFieldsToAlbums < ActiveRecord::Migration
  def change
    # Additional fields for artists
    add_column :artists, :bio, :text
    add_column :artists, :photo, :string
    add_column :artists, :formed_at, :datetime

    # Additional fields for albums
    add_column :albums, :description, :text
    add_column :albums, :artwork, :string
    add_column :albums, :released_at, :datetime

    # Additional fields for tracks
    add_column :tracks, :length, :integer
    add_column :tracks, :artwork, :string
  end
end
