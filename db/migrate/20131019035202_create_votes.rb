class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :user
      t.references :answer
      t.boolean :positive, default: true
      t.timestamps
    end
  end
end
