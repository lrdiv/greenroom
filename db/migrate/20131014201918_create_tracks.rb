class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :title
      t.references :album
      t.references :artist
      t.timestamps
    end
  end
end
