class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.references :user
      t.string :title
      t.boolean :band, default: true
      t.timestamps
    end
  end
end
