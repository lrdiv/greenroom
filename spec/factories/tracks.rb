# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence :title do |n|
    "Track Number |n|"
  end

  factory :track do
    association :artist
    association :album
    title
  end
end
