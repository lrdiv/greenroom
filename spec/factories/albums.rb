# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence :album_title do |n|
    "Album #{n}"
  end

  factory :album do
    association :artist
    title FactoryGirl.generate(:album_title)
    description Lipsum.paragraphs[3]
  end
end
