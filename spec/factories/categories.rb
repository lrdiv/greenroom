# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :category do
    title "#{Lipsum.words[10]}"
    description "#{Lipsum.paragraphs[3]}"
  end
end
