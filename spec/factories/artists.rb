# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence :artist_title do |n|
    "Band #{n}"
  end

  factory :artist do
    association :user
    title FactoryGirl.generate(:artist_title)
    bio "#{Lipsum.paragraphs[3]}"
  end
end
