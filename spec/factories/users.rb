# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence :email do |n|
    "test-#{n}@example.com"
  end

  sequence :username do |n|
    "user-#{n}"
  end

  factory :user do
    username
    email
    password "password"
  end

  factory :friend, parent: :user, class: User do
    username "Dieselnator"
    email "larry2@diesel.com"
  end

  factory :user_invalid, parent: :user do
    username "LarryDiesel"
    email "bob@diesel.com"
    password "password1"
  end
end
