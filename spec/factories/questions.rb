# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question do
    association :user
    association :category
    title "#{Lipsum.words[10]}"
    body "#{Lipsum.paragraphs[3]}"
  end
end
