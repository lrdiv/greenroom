require 'spec_helper'

describe Vote do

  it { should belong_to(:user) }
  it { should belong_to(:answer) }

  context "that is saved" do

    before(:each) do
      @vote = FactoryGirl.build(:vote)
    end

    it "should be valid" do
      @vote.should be_valid
    end
  end

end
