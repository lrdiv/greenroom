require 'spec_helper'

describe Tagging do

  it { should belong_to(:question) }
  it { should belong_to(:tag) }

  context "that is saved" do

    before(:each) do
      @tagging = FactoryGirl.build(:tagging)
    end

    it "should be valid" do
      @tagging.should be_valid
    end
  end
end
