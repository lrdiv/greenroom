require 'spec_helper'

describe Category do

  it { should have_many(:questions) }

  context "that is saved" do

    before(:each) do
      @category = FactoryGirl.build(:category)
    end

    it "should be valid" do
      @category.should be_valid
    end
  end

end
