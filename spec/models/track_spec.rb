require 'spec_helper'

describe Track do

  it { should belong_to(:artist) }
  it { should belong_to(:album) }

  context "that is saved" do

    before(:each) do
      artist = FactoryGirl.build(:artist)
      album = FactoryGirl.build(:album, artist: artist)
      @track = FactoryGirl.build(:track, artist: artist, album: album)
    end

    it "should be valid" do
      @track.artist = @track.album.artist
      @track.should be_valid
    end
  end
end
