require 'spec_helper'

describe Artist do
  
  it { should belong_to(:user) }

  it { should have_many(:albums) }
  it { should have_many(:tracks) }

  context "that is being created" do

    before(:each) do
      @artist = FactoryGirl.build(:artist)
    end

    it "should have a title" do
      @artist.title = ""
      @artist.should_not be_valid
    end
  end
end
