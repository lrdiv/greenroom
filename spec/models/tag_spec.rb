require 'spec_helper'

describe Tag do

  it { should have_many(:taggings) }
  it { should have_many(:questions).through(:taggings) }

  context "that is saved" do
    
    before(:each) do
      @tag = FactoryGirl.build(:tag)
    end

    it "should be valid" do
      @tag.should be_valid
    end
  end
end
