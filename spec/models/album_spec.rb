require 'spec_helper'

describe Album do
  
  it { should belong_to(:artist) }
  it { should have_many(:tracks) }

  context "that is being created" do

    before(:each) do
      @album = FactoryGirl.build(:album)
    end

    it "should be valid" do
      @album.should be_valid
    end
  end
end
