require 'spec_helper'

describe User do

  it { should have_many(:artists) }
  it { should have_many(:friendships) }

  context "that is being created" do

    before(:each) do
      @user = FactoryGirl.build(:user)
    end

    it "should have a unique username" do
      @user.save
      new_user = FactoryGirl.build(:user)
      new_user.email = @user.email
      new_user.should_not be_valid
    end

    it "should have a unique email address" do
      @user.save
      new_user = FactoryGirl.build(:user)
      new_user.username = @user.username
      new_user.should_not be_valid
    end
  end
end
