require 'spec_helper'

describe Question do
  
  it { should belong_to(:user) }
  it { should belong_to(:category) }

  it { should have_many(:answers) }
  it { should have_many(:taggings) }
  it { should have_many(:tags).through(:taggings) }

  context "that is saved" do

    before(:each) do
      @question = FactoryGirl.build(:question)
    end
    
    it "should be valid" do
      @question.should be_valid
    end
  end
end
