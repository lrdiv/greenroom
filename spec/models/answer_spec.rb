require 'spec_helper'

describe Answer do
  
  it { should belong_to(:question) }
  it { should have_many(:votes) }

  context "that is saved" do

    before(:each) do
      @answer = FactoryGirl.build(:answer)
    end

    it "should be valid" do
      @answer.should be_valid
    end
  end
end
