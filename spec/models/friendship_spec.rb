require 'spec_helper'

describe Friendship do

  it { should belong_to(:friend) }
  it { should belong_to(:user) }

  context "that is saved" do

    before(:each) do
      @friendship = FactoryGirl.build(:friendship)
    end

    it "should be valid" do
      @friendship.should be_valid
    end
  end
end
