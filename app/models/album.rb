class Album < ActiveRecord::Base

  # Associations
  # ------------

  belongs_to :artist
  has_many :tracks

end
