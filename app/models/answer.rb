class Answer < ActiveRecord::Base

  belongs_to :question

  has_many :votes

  scope :permitted, -> { where('removed_at IS NULL') }
  
end
