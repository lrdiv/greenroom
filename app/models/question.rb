class Question < ActiveRecord::Base

  belongs_to :user
  belongs_to :category

  has_many :answers
  has_many :taggings
  has_many :tags, through: :taggings

  scope :by_date_desc, -> { order('created_at desc') }
  
end
