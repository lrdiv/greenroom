class Track < ActiveRecord::Base

  # Associations
  # ------------

  belongs_to :album
  belongs_to :artist

end
