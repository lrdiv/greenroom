class User < ActiveRecord::Base
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Associations
  # ------------

  has_many :artists
  has_many :tracks, through: :artists

  has_many :friendships
  has_many :friends, through: :friendships

  # Validations
  # -----------

  validates_presence_of :email, :username
  validates_uniqueness_of :email, :username

end
