class Artist < ActiveRecord::Base

  # Associations
  # ------------

  belongs_to :user

  has_many :albums
  has_many :tracks

  # Validations
  # -----------

  validates_presence_of :title
  validates_uniqueness_of :title

end
