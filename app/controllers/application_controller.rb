class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Hack the Devise filter to accept username
  before_filter :configure_permitted_parameters, if: :devise_controller?
  # User must be authenticated to use application
  before_filter :authenticate_user!


  protected

    # Add username to permitted params
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit :username, :email, :password, :password_confirmation
      end
    end
end
