class HomeController < ApplicationController

  def index
    @questions = Question.all.by_date_desc.limit(25)
  end

end
