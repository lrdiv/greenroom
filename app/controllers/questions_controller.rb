class QuestionsController < ApplicationController

  before_filter :find_object, except: [:index, :new, :create]

  def index
    @questions = Question.all
  end

  def show
    @answers = @question.answers.permitted
    @author = @question.user
  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(permitted_params)
    @question.user = current_user if current_user
    if @question.save
      redirect_to questions_path, notice: "Question submitted."
    else
      redirect_to new_question_path
    end
  end

  def edit
  end

  def update
    if @question.save
      redirect_to root_path, notice: "Question updated."
    else
      redirect_to edit_question_path(@question)
    end
  end

  def destroy
    if @question.destroy
      redirect_to questions_path, notice: "Question deleted."
    else
      redirect_to question_path(@question)
    end
  end

  private

    def find_object
      @question = Question.find(params[:id])
    end

    def permitted_params
      params.require(:question).permit(:title, :body, :user_id, :category_id)
    end
end
