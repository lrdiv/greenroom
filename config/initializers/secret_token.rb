# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Greenroom::Application.config.secret_key_base = '5af4da1276106abc7f91a8df8d360bcddcd5f64a87302cccb751d6bcf72a2d4f9393c67e02d13d748ff653439fbdb7dc7039163e44c4cde9abecb8a9c9849761'
