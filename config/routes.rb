Greenroom::Application.routes.draw do
  
  # Set up Devise with login and logout routes
  devise_for :users, path: "", path_names: {sign_in: "login", sign_out: "logout"}

  # Root path
  root to: "questions#index"

  # Question and answer resources
  resources :questions
  resources :tags
  resources :categories

  # Social resources
  resources :friendships
  resources :artists do
    resources :albums
    resources :tracks
  end
end
